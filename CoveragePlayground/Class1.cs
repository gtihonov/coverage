﻿namespace CoveragePlayground;

public class ObjectWithBehavior
{
    public int State { get; private set; }

    public void IncrementState()
    {
        State++;
    }

    public bool RaiseExceptionIfTooLong(string input)
    {
        if (input == null)
        {
            throw new ArgumentNullException(nameof(input));
        }
        
        if (input.Length >= 10)
        {
            return false;
        }

        return true;
    }
}
public class Class1
{
}