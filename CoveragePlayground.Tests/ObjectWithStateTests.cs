using System;
using Xunit;

namespace CoveragePlayground.Tests;

public class ObjectWithStateTests
{
    [Fact]
    public void ShouldIncrementState()
    {
        var obj = new ObjectWithBehavior();

        var prevState = obj.State;
        
        obj.IncrementState();
        
        Assert.Equal(++prevState, obj.State);
    }

    [Theory]
    [InlineData("s")]
    [InlineData("sssssssssssssssss")]
    [InlineData(null)]
    public void ShouldThrowIfTooLong(string input)
    {
        var obj = new ObjectWithBehavior();

        if (input == null)
        {
            Assert.ThrowsAny<ArgumentNullException>(() => obj.RaiseExceptionIfTooLong(input));
            return;
        }
        if (input.Length >= 10)
        {
            Assert.False(obj.RaiseExceptionIfTooLong(input));
            return;
        }
        
        Assert.True(obj.RaiseExceptionIfTooLong(input));
    }
}